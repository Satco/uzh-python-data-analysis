# -*- coding: utf-8 -*-

# Get the total number of countries given by the http://api.population.io/#!/countries
# Use the python "requests" Library

import os.path
import pickle
import requests
from solutions.population_exercise.api_name import get_api_name

api_name = get_api_name()

countries = requests.get("http://" + api_name + ":80/1.0/countries")
countries_list = countries.json()["countries"]


def get_clean_country_list():
    pickle_file = 'all_country_list.pkl'

    if os.path.isfile(pickle_file):
        clean_country_list = pickle.load(open(pickle_file, 'rb'))
        return clean_country_list

    clean_country_list = []
    black_list_country = [u'Least developed countries',
                          u'World',
                          u'Less developed regions, excluding least developed countries']
    for country in countries_list:
        if country in black_list_country:
            continue
        response = requests.get(
            "http://" + api_name + ":80/1.0/life-expectancy/remaining/male/{}/2001-05-11/49y2m/".format(country))
        if response.status_code == 200:
            clean_country_list.append(country)

    pickle.dump(clean_country_list, open(pickle_file, 'wb'))
    return clean_country_list


def run_exercise():
    clean_country_list = get_clean_country_list()
    print(clean_country_list)
    print("There are " + str(len(countries_list)) + " countries available in the population.io")


if __name__ == '__main__':
    run_exercise()