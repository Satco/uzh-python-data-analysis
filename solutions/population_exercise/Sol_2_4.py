# -*- coding: utf-8 -*-

# Get the top 10 countries with the highest life expectancy (average woman/man) for kids born on 2015-06-30
# given the function from Sol_2_1
# and http://api.population.io/#!/life-expectancy/calculateTotalLifeExpectancy
# Use the python "requests" Library for the http requests

import requests
from solutions.population_exercise import Sol_2_1
from solutions.population_exercise.api_name import get_api_name

api_name = get_api_name()

countries_list = Sol_2_1.get_clean_country_list()


def get_the_top_n(sort_list, n, sort_argument, reverse):
    """Sorts the given list according to the sort argument, return the n top entries in a list

    Keyword arguments:
    sort_list       -- The unsorted list which has to be sorted
    n               -- The number of names in the display
    sort_argument   -- lambda argument with the info what key to sort
    reverse         -- True for descending False for ascending
    """
    sort_list = sorted(sort_list, key=sort_argument, reverse=reverse)
    top_sort_list = []

    counter = 0
    while counter < n:
        top_sort_list.append([sort_list[counter][0], str(sort_list[counter][1])])
        counter += 1

    return top_sort_list


def print_2d_list(print_list):
    """Prints the given 2d list slightly formatted

    Keyword arguments:
    print_list -- the list which has to be printed
    """

    for item in print_list:
        # this command
        print(' - '.join(item))


def get_life_expectancy(sex, country):
    """Returns the life expectancy for the given arguments for children born on 2015-06-30.

    Keyword arguments:
    sex     -- female or male
    country -- the name of the country (defined by population.io)
    """
    date = "2015-06-30"
    data = requests.get("http://" + api_name + ":80/1.0/life-expectancy/total/" + sex + "/" + country + "/" + date + "/")

    return data.json()["total_life_expectancy"]


def get_average_life_expectancy(country):
    """Returns the average life expectancy for the given country.

    Keyword arguments:
    country -- the name of the country (defined by population.io)
    """
    return (get_life_expectancy("female", country) + get_life_expectancy("male", country)) / 2.



list_to_sort = []
# containing sublists [['Afghanistan', 77.3009302725], ['Switzerland', 92.7132960888]...]

# iterating over countries and determine the life expectancies
for country in countries_list:
    print("Processing: " + country)

    average_life_expectancy = get_average_life_expectancy(country)

    # Calculating relative population growth from today to tomorrow
    country_list = [country, average_life_expectancy]
    list_to_sort.append(country_list)

# calling the sorting function
key = (lambda land: land[1])
top_list = get_the_top_n(list_to_sort, 10, key, True)

# calling the printing function
print_2d_list(top_list)