import pandas as pd
import matplotlib.pyplot as plt

# In case you want to know more about the individual detection methods
# https://en.wikipedia.org/wiki/Methods_of_detecting_exoplanets
planets = pd.read_csv("https://raw.githubusercontent.com/mwaskom/seaborn-data/master/planets.csv")
print(planets.head())
print(planets.describe())
# 1. Make a boxplot of each column which allows for it
for column in planets.columns:
    if column != "method":
        plt.figure()
        planets.boxplot(column=column)
# 2. Make a piechart of the methods column
plt.figure()
planets.method.value_counts().plot(kind='pie')
plt.show()