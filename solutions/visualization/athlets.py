import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

df = pd.read_csv("athlete_events.csv")
print(df.info())

# 1. Show the top 3 athletes who won the most medals and how many they won
print(pd.DataFrame(df.Name.value_counts()).head(3))


# 2. write a function which takes a name as input and outputs how often the athlete joined the olympics in what year they joined for the first time, what their age was
# at their first event was. The gender, how many medals they won in total, from what country they are and how many times they competed in the olympics
def player_summary(name):
    years_joined = df[df.Name == name].Year.unique()
    min_age = int(df[df.Name == name].Age.min())
    medals = sum(df[df.Name == name].Medal.value_counts())
    nation = df[df.Name == name].Team.unique()
    if 'M' == df[df.Name == name].Sex.unique():
        gender = 'He'
    else:
        gender = 'She'

    print('{0} joined Olympics in {1} for the first time when {4} was {2} years old. {5} earned {3} medals in total. {5} represented {6} for {7} times.'
          .format(name, years_joined.min(), min_age, medals, gender.lower(), gender, nation[0], len(years_joined)))


# 3. use the function on the top 3 athlets from before
x = pd.DataFrame(df.Name.value_counts()).head(3)
for name, athlete in pd.DataFrame(df.Name.value_counts()).head(3).iterrows():
    player_summary(name)

# 4. Plot how many male and female athletes competed in the Olympics in a boxplot. hint: sns.countplot
sns.countplot(x=df.Sex, label='Count')
M, F = df.Sex.value_counts()
print('Since {0} to {1}, {2} female and {3} male atheletes competed in Olympics.'.format(df.Year.min(), df.Year.max(), F, M))

# 5. Make two scatter plots showing the number of male/female athletes  for each year of the olympics
print(df.groupby('Year')['Sex'].value_counts())
print(df.groupby('Year')['Sex'].value_counts().unstack())
print(pd.DataFrame(df.groupby('Year')['Sex'].value_counts().unstack()))
Year_Sex = pd.DataFrame(df.groupby('Year')['Sex'].value_counts().unstack())
Year_Sex = Year_Sex.reset_index()
print(Year_Sex)

Year_Sex.plot(kind="scatter", x='Year', y='F')
Year_Sex.plot(kind="scatter", x='Year', y='M')
plt.show()

# 6. Plot the average height of male/female athletes per year
Year_Height = pd.DataFrame(df.groupby(['Year', 'Sex'])['Height'].mean().unstack(fill_value=0))
Year_Height = Year_Height.reset_index()

sns.lineplot(x="Year", y="M", data=Year_Height).set_title('Average height of male atheletes')
sns.lineplot(x="Year", y="F", data=Year_Height).set_title('Average height of female atheletes')

# 7. Do the same for another column of your choice
Year_Weight = pd.DataFrame(df.groupby(['Year', 'Sex'])['Weight'].mean().unstack(fill_value=0))
Year_Weight = Year_Weight.reset_index()
sns.lineplot(x="Year", y="M", data=Year_Weight).set_title('Average weight of male atheletes')
sns.lineplot(x="Year", y="F", data=Year_Weight).set_title('Average weight of female atheletes')
