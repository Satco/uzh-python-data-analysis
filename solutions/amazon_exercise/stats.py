import statistics


def print_stats(data, description):
    print('The mean {} is: {}'.format(description, statistics.mean(data)))
    print('The median {} is: {}'.format(description, statistics.median(data)))
    print('The mode of {} is: {}'.format(description, statistics.mode(data)))
    if len(data) > 1:
        print('The standard deviation of {} is: {}'.format(description, statistics.stdev(data)))
    else:
        print("Not applicable")

    print('\n')


def print_review_statistics_per_product(reviews):
    """
         Print the mean, median, mode and standard deviation of the review scores and review text lengths(number of words) per product
        """
    products = {}
    for review in reviews:
        current_product_id = review["product/productId"]
        if current_product_id in products:
            products[current_product_id]["scores"].append(review['review/score'])
            products[current_product_id]["review_lengths"].append(len(review['review/text'].split(' ')))
        else:
            data = {"scores": [review['review/score']],
                    "review_lengths": [len(review['review/text'].split(' '))]}
            products[current_product_id] = data

    counter = 0
    for product_id, data in products.items():
        if counter < 5:
            print("Number of reviews: {}".format(len(data["scores"])))
            print_stats(data["scores"], str(product_id) + " scores")
            print_stats(data["review_lengths"], str(product_id) + " review_lenghts")
        counter += 1


def print_review_statistics(reviews):
    """
     Print the mean, median, mode and standard deviation of the review scores and review text lengths(number of words)
    """
    scores = []
    review_lengths = []

    for review in reviews:
        scores.append(review['review/score'])
        review_lengths.append(len(review['review/text'].split(' ')))

    print_stats(scores, 'score')
    print_stats(review_lengths, 'review length')
