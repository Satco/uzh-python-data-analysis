# Use the pygal radar (see http://www.pygal.org/en/latest/documentation/types/radar.html) to plot:
#   - Relative Population (to the world),
#   - Life Expectancy for Women                     /100
#   - Life Expectancy of Men'                       /100
#   - Life ExExpectancy A of both women and men     /100
#   - Relative Population Growth
#
# Of the following countries:
#   - "Ghana", "Sudan", "Libya", "South Africa", "Sierra Leone", "Cabo Verde", "Tanzania" and "Angola"
#
# Get the data via http request from the population.io api.
# http://api.population.io:80/1.0/life-expectancy/total/" + sex + "/" + country + "/" + date + "/"
# and http://api.population.io:80/1.0/population/{COUNTRY}/today-and-tomorrow/
# Use the python "requests" Library
#
# Check the the life expectancy of the 1st of January 2015 for all countries
#
# When you are done, feel free to compare other countries from http://api.population.io/1.0/countries
# Try to improve the view of the relative population.

import pygal
import requests

countries_list = ["Ghana", "Sudan", "Libya", "South Africa", "Sierra Leone", "Cabo Verde", "Tanzania"]

x_labels = ['Rel. Population', 'Life Ex. F', 'Life Ex. M', 'Life Ex. A', 'Rel. Pop. Growth']

