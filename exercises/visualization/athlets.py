import pandas as pd
import seaborn as sns
import matplotlib.pyplot as plt

df = pd.read_csv("athlete_events.csv")
print(df.info())

# 1. Show the top 3 athletes who won the most medals and how many they won


# 2. write a function which takes a name as input and outputs how often the athlete joined the olympics in what year they joined for the first time, what their age was
# at their first event was. The gender, how many medals they won in total, from what country they are and how many times they competed in the olympics
def player_summary(name):
    pass


# 3. use the function on the top 3 athlets from before

# 4. Plot how many male and female athletes competed in the Olympics in a boxplot. hint: sns.countplot

# 5. Make two scatter plots showing the number of male/female athletes  for each year of the olympics

# 6. Plot the average height of male/female athletes per year

# 7. Do the same for another column of your choice
