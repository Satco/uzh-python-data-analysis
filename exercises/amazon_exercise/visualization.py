import matplotlib
import pygal
from wordcloud import WordCloud

matplotlib.use('TKAgg')
import matplotlib.pyplot as pyplot


def create_wordcloud(word_list, filename):
    """
     Saves image of a wordcloud
     Documentation: https://github.com/amueller/word_cloud
    :param word_list: list of strings
    :param filename: string
    """
    pyplot.clf()

    # Implementation...
    # ...
    # ...
    # ...


def create_review_length_boxplot(reviews, filename, box_mode='tukey'):
    """
     Creates a boxplot chart with five entries that show the distribution of the review lengths for all possible scores
     Reviews longer than 1500 words are ignored in this example
     Chart documentation: http://www.pygal.org/en/latest/documentation/types/box.html
    :param reviews: list of review dictionaries
    :param filename: string
    :param box_mode: type of boxchart, e.g. None, 'tukey', 'stdv'
    """
    one_star_review_lengths = []
    two_star_review_lengths = []
    three_star_review_lengths = []
    four_star_review_lengths = []
    five_star_review_lengths = []

    # Implementation...
    # ...
    # ...
    # ...

    # Use boxplot.render_to_file(filename) to save chart


def create_score_barchart(reviews, filename):
    """
     Creates a barchart that shows the distribution of the scores, i.e. how many 1-star reviews, 2-star reviews, etc.
     Chart documentation: http://www.pygal.org/en/latest/documentation/types/bar.html
    :param reviews: list of review dictionaries
    :param filename: string
    """

    # Implementation...
    # ...
    # ...
    # ...

    # Use line_chart.render_to_file(filename) to save chart
