# -*- coding: utf-8 -*-

# Get the top 10 countries with the highest life expectancy (average woman/man) for kids born on 2015-06-30
# given by the function form Sol_2_1
# and http://api.population.io/#!/life-expectancy/calculateTotalLifeExpectancy
# Use the python "requests" Library for the http requests

import requests

from exercises.population_exercise.population_1 import get_clean_country_list


def get_life_expectancy(dob, gender, country):
    url = 'http://api.population.io:80/1.0/life-expectancy/total/{}/{}/{}/'.format(gender, country, dob)
    response = requests.get(url)
    return response.json()['total_life_expectancy']


if __name__ == '__main__':
    country_average_life_expectancies = []

    clean_country_list = get_clean_country_list()
    for country in clean_country_list:
        male_life_expectancy = get_life_expectancy('2015-06-30', 'male', country)
        female_life_expectancy = get_life_expectancy('2015-06-30', 'female', country)
        average_life_expectancy = (female_life_expectancy + male_life_expectancy) / 2

        country_average_life_expectancies.append((country, average_life_expectancy))

    top_10_le = sorted(country_average_life_expectancies, key=lambda row: row[1], reverse=True)[:10]

    for country in top_10_le:
        print("{}: {}".format(country[0], country[1]))
