# -*- coding: utf-8 -*-

# Get the correlation between the relative growth an life expectancy of the countries
# given by the function form Sol_2_1
# and http://api.population.io/#!/life-expectancy/calculateTotalLifeExpectancy
# Use the python "requests" Library for the http requests
