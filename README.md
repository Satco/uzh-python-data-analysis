# Datenanalyse und Datenvisualisierung mit Python #

Repository für den IT Fort- und Weiterbildungskurs an der Universität Zürich

## Unterlagen ##

Alle Unterlagen befinden sich im resources Ordner von diesem Repository

## PyCharm ##

- PyCharm ist eine weitverbreitete Entwicklungsumgebung für Python. Die Community Edition ist gratis und kann hier heruntergeladen werden: https://www.jetbrains.com/pycharm/download/

- Hilfe zur Installation und Nutzung von PyCharm findet ihr hier (in Englisch): https://www.jetbrains.com/pycharm/help/meet-pycharm.html

- Hilreiche Einführungsvideos zu PyCharm findet ihr hier: https://www.youtube.com/playlist?list=PLQ176FUIyIUZ1mwB-uImQE-gmkwzjNLjP

##### Zu beachten für Windows Benutzer #####

Um auf Windows Python ausführen zu können, müsst ihr zuerst den neusten Python Interpreter installieren. Dieser könnt ihr hier herunterladen: https://www.python.org/downloads/windows/

